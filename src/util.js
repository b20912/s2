/*Sample Functions to Test*/

function getCircleArea(radius){

	//area = pi * r^2

	return 3.1416*(radius**2);

}

function checkIfPassed(score,total){

	return (score/total)*100 >= 75;
}

function getAverage(num1,num2,num3,num4){

	return (num1+num2+num3+num4)/4;

}

function getSum(num1,num2){

	return num1+num2;
}

function getDifference(num1,num2){

	return num1-num2;
}

function divCheck(num){

	if(num % 5 === 0){
		return true;
	} 
	else if (num % 7 === 0){
		return true;
	} 
	else {
		return false;
	}
}

function isOddOrEven(num){
	if(num % 2 === 0){
		return "even";
	} else {
		return "odd"
	}
}

function reverseString(string){
	let loopString = "";

	for(let i = string.length -1; i >= 0; i--){
		loopString += string[i];
	}

	return loopString;
}

//reading list

function longestString(array){
	let stringResult = "";

	 array.forEach(function(string) {
	 if(string.length > stringResult.length) {
	 	stringResult = string;
	 	}
	});
	return stringResult;
}

function numberOfVowels(string){
	let vowels = ["a","e","i","o","u"];

	let count = 0;

	for(let letter of string.toLowerCase()) {
		if (vowels.includes(letter)){
			count ++;
		}
	}

	return count;
}

function withinProfit(expense){
	let income = 1000000;

	if(expense <= income){
		return true;
	} else {
		return false;
	}
}

module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum,getSum,
	getDifference: getDifference,
	divCheck: divCheck,
	isOddOrEven: isOddOrEven,
	reverseString: reverseString,
	longestString: longestString,
	numberOfVowels: numberOfVowels,
	withinProfit: withinProfit
}