const { getCircleArea,checkIfPassed,getAverage,getSum,getDifference,divCheck,isOddOrEven,reverseString,longestString,numberOfVowels,withinProfit } = require('../src/util.js');
//import the assert statements from our chai
const { expect,assert } = require('chai');

//test case - a conditin we are testing
//it(stringExplainsWhatTheTestDoes,functionToTest)
//assert is used to assert conditions for the test to pass. If the assertion fails then the test is considered failed
//describe() is used to create a test suite. A test suit is a group of test cases related to one another or tests the same method, data or function.

describe('test_get_area_circle_area',() => {

	it('test_area_of_circle_radius_15_is_706.86',() =>{

		let area = getCircleArea(15);
		assert.equal(area,706.86);
	});

	it('test_area_of_circle_radius_300_is_282744',() =>{

		let area = getCircleArea(300);
		expect(area).to.equals(282744);
	});

});

describe('test_check_if_passed',()=>{

	it('test_25_out_of_30_if_passed',()=>{
		
		let isPassed = checkIfPassed(25,30)
		assert.equal(isPassed,true);
	});

	it('test_30_out_of_50_is_not_passed',()=>{

		let isPassed = checkIfPassed(30,50)
		assert.equal(isPassed,false);
	});
});

describe('test_get_ave_grades',() => {

	it('test_if_ave_of_80_82_84_86_is_83',()=>{

		let aveGrade = getAverage(80,82,84,86)
		expect(aveGrade).to.equals(83);
	});

	it('test_if_ave_of_70_80_82_84_is_79',()=>{

		let aveGrade = getAverage(70,80,82,84)
		expect(aveGrade).to.equals(79);
	});
});

describe('test_get_sum', () => {

	it('test_sum_of_15_30_is_45',() =>{

		let sum = getSum(15,30)
		assert.equal(sum,45);
	});

	it('test_sum_of_25_50_is_75',() =>{

		let sum = getSum(25,50)
		assert.equal(sum,75);
	});
});

describe('test_get_difference', () => {

	it('test_difference_of_70_40_is_30',() =>{

		let difference = getDifference(70,40)
		assert.equal(difference,30);
	});

	it('test_sum_of_125_50_is_75',() =>{

		let difference = getDifference(125,50)
		assert.equal(difference,75);
	});
});


describe('test_divCheck', () => {

	it('test_if_30_is_div_by_5_or_7',() =>{

		let numCheck = divCheck(30)
		assert.equal(numCheck,true);
	});

	it('test_if_72_is_div_by_5_or_7',() =>{

		let numCheck = divCheck(72)
		assert.equal(numCheck,false);
	});

	it('test_if_21_is_div_by_5_or_7',() =>{

		let numCheck = divCheck(21)
		assert.equal(numCheck,true);
	});

	it('test_if_23_is_div_by_5_or_7',() =>{

		let numCheck = divCheck(23)
		assert.equal(numCheck,false);
	});
});

describe('test_if_num_isOddOrEven', () => {

	it('test_if_21_is_odd',() =>{

		let numCheck = isOddOrEven(21)
		assert.equal(numCheck,"odd");
	});

	it('test_if_72_is_even',() =>{

		let numCheck = isOddOrEven(72)
		assert.equal(numCheck,"even");
	});

	it('test_if_99_is_odd',() =>{

		let numCheck = isOddOrEven(99)
		assert.equal(numCheck,"odd");
	});

	it('test_if_86_is_even',() =>{

		let numCheck = isOddOrEven(86)
		assert.equal(numCheck,"even");
	});
});

describe('test_reverseString',() => {

	it('test_reverse_of_hello_is_olleh',() => {

		assert.equal(reverseString('hello'),"olleh");
	});

	it('test_reverse_of_hi_is_ih',() => {

		assert.equal(reverseString('hi'),"ih");
	});

	it('test_reverse_of_irene_is_eneri',() => {

		assert.equal(reverseString('irene'),"eneri");
	});

	it('test_reverse_of_seulgi_is_iglues',() => {

		assert.equal(reverseString('seulgi'),"iglues");
	});

})

describe('test_longestString',() => {
	it('test_longest_string_of_hello_hi_hey_greetings',() => {
		let array = longestString(["hello","hi","hey","greetings"]);
		assert.equal(array,"greetings");
	});

	it('test_longest_string_of_irene_seulgi_yeri_wendy_joy',() => {
		let array = longestString(["irene","seulgi","yeri","wendy","joy"]);
		assert.equal(array,"seulgi");
	});

	it('test_longest_string_of_yoona_jessica_sooyoung_taeyeon',() => {
		let array = longestString(["yoona","jessica","sooyoung","taeyeon"]);
		assert.equal(array,"sooyoung");
	});
})

describe('test_numberOfVowels',() => {
	it('test_if_hello_has_2_vowels',() => {
		assert.equal(numberOfVowels("hello"),2);
	});

	it('test_if_amazing_has_3_vowels',() => {
		assert.equal(numberOfVowels("amazing"),3);
	});

	it('test_if_taeyeon_has_4_vowels',() => {
		assert.equal(numberOfVowels("taeyeon"),4);
	});
})

describe('test_withinProfit',() => {
	it('test_if_350000_is_withinProfit',() => {
		assert.equal(withinProfit(350000),true);
	});

	it('test_if_1200000_is_not_withinProfit',() => {
		assert.equal(withinProfit(1200000),false);
	});

	it('test_if_1000000_is_withinProfit',() => {
		assert.equal(withinProfit(1000000),true);
	});
})



